<!DOCTYPE html>
<html lang="pt-BR">
<body>
    <a href="https://www.sagacious.club">
        <img alt="logo" align="right" src="https://i.postimg.cc/J4JfDD9c/Pandora-Logo-Circle.png" height="185" width="185"/>
    </a>
    <h1>Aurora</h1>
    <p><b>Aurora is a modular plugin that has a powerful API and implements
    the Spring Framework in Bukkit.</b></p>
    <hr>
    <h2>Usage</h2>
</body>
</html>

First, make sure the repository is in your `pom.xml`:

```xml
<repository>
    <id>sagacious</id>
    <url>https://artifactory.nubraza.com/artifactory/libs-release-local/</url>
</repository>
```

Then, make sure the dependency is in your `pom.xml`:

```xml
<dependency>
    <groupId>com.gitlab.pk7r</groupId>
    <artifactId>aurora-api</artifactId>
    <version>2.1</version>
    <scope>provided</scope>
</dependency>
```



To create a module define the `module` annotation with the parameter of the module name:

```java
@Module("warp")
public class WarpModule {

}
```

You need to annotate the command and listener classes with `@Component` for the system to identify them.
For the commands to be registered, use `@PostConstruct` at command methods.

### Creating commands

To create a command, just create a class similar to this example:

```java
@Command
public class BroadcastCommand {

    @Autowired
    private Server server;

    @PostConstruct //Required to register command
    public void registerCommandOnInit() {
        new CommandAPICommand("broadcast")
                .withAliases("bc")                                  // Command aliases
                .withPermission("broadcast.admin")                  // Required permissions
                .executes((sender, args) -> {
                    if (!(sender instanceof Player)) return;
                    StringBuilder builder = new StringBuilder();
                    for (String args : args) {
                        builder.append(arg).append(" ");
                    }
                    server.broadcastMessage(builder.toString());
                    // and whatever method you want for the command
                }).register();
    }
}
```

More examples are available in [CommandAPI](https://commandapi.jorel.dev/6.0.0/commandregistration.html)
(thanks to [Jorel](https://jorel.dev/)).

### Creating listeners

To create a listener, just create a class similar to this example:

```java
@Event
public class LoginListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
    // your code here
    }
}
```
