package com.gitlab.pk7r.aurora.hook;

import org.bukkit.entity.Player;

public interface ChatHook {

    String getSuffix(Player player);

    String getPrefix(Player player);

}