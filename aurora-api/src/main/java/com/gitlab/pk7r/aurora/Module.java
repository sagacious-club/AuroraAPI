package com.gitlab.pk7r.aurora;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@ComponentScan
@Documented
@Component
public @interface Module {
    @AliasFor(
            annotation = Component.class
    )
    String value();
}