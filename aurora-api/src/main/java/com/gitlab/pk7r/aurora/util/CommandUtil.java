package com.gitlab.pk7r.aurora.util;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.plugin.SimplePluginManager;

public class CommandUtil {

    public static void unregisterCommands(List<String> commands) {
        if (Bukkit.getServer().getPluginManager() instanceof SimplePluginManager) {
            final SimplePluginManager manager = (SimplePluginManager) Bukkit.getServer().getPluginManager();
            for (String command : commands) {
                try {
                    Field commandMapField = SimplePluginManager.class.getDeclaredField("commandMap");
                    commandMapField.setAccessible(true);
                    CommandMap commandMap = (CommandMap) commandMapField.get(manager);
                    Field knownCommandsField = SimpleCommandMap.class.getDeclaredField("knownCommands");
                    knownCommandsField.setAccessible(true);
                    @SuppressWarnings("unchecked")
                    Map<String, Command> knownCommands = (Map<String, Command>) knownCommandsField.get(commandMap);
                    for (Map.Entry<String, Command> entry : knownCommands.entrySet()) {
                        if (entry.getKey().equals(command)) {
                            entry.getValue().unregister(commandMap);
                        }
                    }
                    knownCommands.remove(command);
                } catch (IllegalArgumentException
                        | NoSuchFieldException
                        | IllegalAccessException
                        | SecurityException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
