package com.gitlab.pk7r.aurora.model;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.function.Consumer;

public class MenuItem {

    private ItemStack icon;

    private String name;

    private Consumer<InventoryClickEvent> action;

    public ItemStack getIcon() {
        return icon;
    }

    public void setIcon(ItemStack icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Consumer<InventoryClickEvent> getAction() {
        return action;
    }

    public void setAction(Consumer<InventoryClickEvent> action) {
        this.action = action;
    }

    public MenuItem(ItemStack icon, String name, Consumer<InventoryClickEvent> action) {
        this.icon = icon;
        this.name = name;
        this.action = action;
    }
}