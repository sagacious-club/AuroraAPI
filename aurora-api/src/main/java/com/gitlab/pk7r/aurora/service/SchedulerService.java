package com.gitlab.pk7r.aurora.service;

public interface SchedulerService {

    int scheduleSyncDelayedTask(Runnable task, long delay);

    int scheduleSyncDelayedTask(Runnable task);

    int scheduleSyncRepeatingTask(Runnable task, long delay, long period);

    void cancelTask(int taskId);

    boolean isCurrentlyRunning(int taskId);

    boolean isQueued(int taskId);
}
