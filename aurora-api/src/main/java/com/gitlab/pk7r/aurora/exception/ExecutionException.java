package com.gitlab.pk7r.aurora.exception;

import com.gitlab.pk7r.aurora.util.Message;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class ExecutionException extends RuntimeException {

    public ExecutionException(CommandSender sender, String message) {
        super(message, new Throwable());
        sender.spigot().sendMessage(new Message(ChatColor.RED + message).formatted());
    }
}