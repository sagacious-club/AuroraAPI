package com.gitlab.pk7r.aurora.execution;


import com.gitlab.pk7r.aurora.exception.ExecutionException;
import org.springframework.scheduling.annotation.Async;

import java.util.concurrent.CompletableFuture;

public class Execution {

    @Async
    public <T> T run(ExecutionRunnable<T> runnable) {
        try {
            return runnable.run();
        } catch (ExecutionException ignored) {}
        return null;
    }
}