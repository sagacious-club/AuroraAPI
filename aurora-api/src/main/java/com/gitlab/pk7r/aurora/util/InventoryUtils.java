package com.gitlab.pk7r.aurora.util;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import static com.gitlab.pk7r.aurora.util.ItemUtils.isEmpty;
import static java.util.Arrays.stream;

public interface InventoryUtils {

    static boolean isInventoryEmpty(Inventory inventory) {
        if (inventory == null) return true;
        return stream(inventory.getContents()).allMatch(ItemUtils::isEmpty);
    }

    static boolean isInventoryEmpty(PlayerInventory inventory) {
        if (inventory == null) return true;
        return isEmpty(inventory.getItemInMainHand())
                && isEmpty(inventory.getItemInOffHand())
                && isInventoryEmpty((Inventory) inventory)
                && stream(inventory.getArmorContents()).allMatch(ItemUtils::isEmpty);
    }

    static void clearInventory(PlayerInventory inventory) {
        if (inventory == null) return;
        inventory.clear();
        inventory.setItemInOffHand(null);
        inventory.setHelmet(null);
        inventory.setChestplate(null);
        inventory.setLeggings(null);
        inventory.setBoots(null);
    }

    static void safeAdd(Player player, ItemStack item) {
        if (isEmpty(item)) return;
        World world = player.getWorld();
        Location location = player.getLocation().clone();
        player.getInventory().addItem(item).values()
                .forEach(exceeded -> world.dropItemNaturally(location, exceeded));
    }
}
