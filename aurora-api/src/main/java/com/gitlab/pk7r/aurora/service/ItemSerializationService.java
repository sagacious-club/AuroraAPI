package com.gitlab.pk7r.aurora.service;

import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.stream.Stream;

public interface ItemSerializationService {

    Stream<ItemStack> deSerialize(String serialized);

    ItemStack deSerializeSingle(String serialized);

    String serialize(ItemStack[] items);

    String serialize(List<ItemStack> items);

    String serialize(ItemStack item);

}