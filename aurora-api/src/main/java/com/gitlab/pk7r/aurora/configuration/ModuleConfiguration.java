package com.gitlab.pk7r.aurora.configuration;

import org.bukkit.configuration.ConfigurationSection;

public interface ModuleConfiguration extends ConfigurationSection {

    void reload();

    void save();

}