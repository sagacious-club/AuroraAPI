package com.gitlab.pk7r.aurora.configuration;

public interface FileConfiguration {

    ModuleConfiguration getConfiguration(String filename);
}