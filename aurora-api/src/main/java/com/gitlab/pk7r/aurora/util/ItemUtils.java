package com.gitlab.pk7r.aurora.util;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public interface ItemUtils {

    static boolean isEmpty(ItemStack item) {
        return item == null
                || Material.AIR.equals(item.getType())
                || "CAVE_AIR".equals(item.getType().name())
                || "VOID_AIR".equals(item.getType().name());
    }

}
