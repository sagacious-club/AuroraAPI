package com.gitlab.pk7r.aurora.service;

import com.gitlab.pk7r.aurora.util.Message;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.UUID;

@Component
public class TemporaryCommandService {

    protected static HashMap<UUID, Runnable> actions = new HashMap<>();

    public static void createTemporaryCommand(Runnable r, Plugin plugin, int expires, Player player) {
        final UUID uuid = UUID.randomUUID();
        actions.put(uuid, r);
        if (expires > 0) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () ->
                    actions.remove(uuid), 20L * expires);
        }
        String tempCommand = "/chataction " + uuid;
        player.spigot().sendMessage(new Message(String.format(
                "&cTem certeza que deseja executar esta ação? &a[[Confirmar]](%s hover=&aClique para Confirmar)", tempCommand)).formatted());
    }
}