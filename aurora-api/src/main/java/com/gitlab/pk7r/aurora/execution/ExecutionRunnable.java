package com.gitlab.pk7r.aurora.execution;

import com.gitlab.pk7r.aurora.exception.ExecutionException;

public interface ExecutionRunnable<T> {

    T run() throws ExecutionException;

}