package com.gitlab.pk7r.aurora.service;

import org.bukkit.Location;

public interface LocationSerializationService {

    String serializeLocation(Location location);

    Location deserializeLocation(String location);

}