#!/bin/sh

set -e

PLUGIN_VERSION=$(mvn -q -Dexec.executable="echo" -Dexec.args='${project.version}' --non-recursive exec:exec)
JAR_FILE=$(realpath $(find ./aurora-impl/target -type f -name "Aurora.jar"))
PLUGIN_NAME=$(mvn -q -Dexec.executable="echo" -Dexec.args='${project.name}' --non-recursive exec:exec)
COMMIT_MESSAGE=$(echo $(git log -1 --pretty=%B))
WEBHOOK_CONFIG="{\"username\": \"Aurora\", \"avatar_url\": \"https://i.postimg.cc/J4JfDD9c/Pandora-Logo-Circle.png\"}"
MESSAGE="{\"username\": \"Aurora\", \"avatar_url\": \"https://i.postimg.cc/J4JfDD9c/Pandora-Logo-Circle.png\", \"content\": null, \"embeds\":
[{\"title\": \"Novo plugin disponível :coffee:\",\"description\": \"$COMMIT_MESSAGE\",\"color\": 33023,\"fields\":
[{\"name\": \"Plugin\",\"value\": \"$PLUGIN_NAME\",\"inline\": true},
{\"name\": \"Versão\",\"value\": \"$PLUGIN_VERSION\",\"inline\": true}]}]}"

curl -f \
    -X POST \
    -F "payload_json=$MESSAGE" \
    $WEBHOOK_URL

# Only run if server contains +7 boosts
#curl -f \
#    -X POST \
#    -F "payload_json=$WEBHOOK_CONFIG" \
#    -F "arquivo=@$JAR_FILE" \
#    $WEBHOOK_MAIN_URL
#