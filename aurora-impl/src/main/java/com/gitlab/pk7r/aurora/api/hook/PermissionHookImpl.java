package com.gitlab.pk7r.aurora.api.hook;

import com.gitlab.pk7r.aurora.hook.PermissionHook;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Service
public class PermissionHookImpl implements PermissionHook {

    Server server;

    Permission permission;

    @PostConstruct
    private void init() {
        permission = server.getServicesManager().getRegistration(Permission.class).getProvider();
    }

    @Autowired
    public PermissionHookImpl(Server server) {
        this.server = server;
    }

    @Override
    public boolean hasPermission(Player player, String permission) {
        return this.permission.has(player, permission);
    }

    @Override
    public void playerAdd(Player player, String permission) {
        this.permission.playerAdd(player, permission);
    }

    @Override
    public void playerRemove(Player player, String permission) {
        this.permission.playerRemove(player, permission);
    }

    @Override
    public void groupAdd(String group, String permission) {
        this.permission.groupAdd((String) null, group, permission);
    }

    @Override
    public void groupRemove(String group, String permission) {
        this.permission.groupRemove((String) null, group, permission);
    }

    @Override
    public boolean isPlayerInGroup(Player player, String groupName) {
        return this.permission.playerInGroup(player, groupName);
    }

    @Override
    public void addPlayerToGroup(Player player, String groupName) {
        this.permission.playerAddGroup(player, groupName);
    }

    @Override
    public void removePlayerFromGroup(Player player, String groupName) {
        this.permission.playerRemoveGroup(player, groupName);
    }

    @Override
    public Set<String> getPlayerGroups(Player player) {
        return new HashSet<>(Arrays.asList(this.permission.getPlayerGroups(player)));
    }

    @Override
    public String getPrimaryGroup(Player player) {
        return this.permission.getPrimaryGroup(player);
    }

    @Override
    public Set<String> getGroups() {
        return new HashSet<>(Arrays.asList(this.permission.getGroups()));
    }
}
