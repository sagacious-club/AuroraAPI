package com.gitlab.pk7r.aurora.loader;

import com.gitlab.pk7r.aurora.Module;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.reflections.Reflections;
import org.reflections.scanners.Scanners;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.Collection;

import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;

@AllArgsConstructor
public class ModuleLoader {

    private final ClassLoader classLoader;

    public Collection<Class<?>> loadModules(File folder) {
        Arrays.stream(defaultIfNull(folder.listFiles(), new File[0]))
                .filter(file -> file.getName().endsWith(".jar"))
                .forEach(this::loadModule);
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .addClassLoaders(classLoader)
                .setUrls(ClasspathHelper.forPackage("com.gitlab.pk7r"))
                .filterInputsBy(new FilterBuilder().includePackage("com.gitlab.pk7r.aurora"))
                .setScanners(Scanners.SubTypes, Scanners.TypesAnnotated));
        return reflections.getTypesAnnotatedWith(Module.class);
    }

    @SneakyThrows
    private void loadModule(File file) {
        try {
            URL jarUrl = file.toURI().toURL();
            Method method = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
            method.setAccessible(true);
            method.invoke(classLoader, jarUrl);
        } catch (IllegalAccessException ignored) {}
    }
}