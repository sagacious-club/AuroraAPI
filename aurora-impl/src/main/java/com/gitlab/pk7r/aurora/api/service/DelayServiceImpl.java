package com.gitlab.pk7r.aurora.api.service;

import com.gitlab.pk7r.aurora.exception.ExecutionException;
import com.gitlab.pk7r.aurora.service.DelayService;
import com.gitlab.pk7r.aurora.service.MemoryService;
import com.gitlab.pk7r.aurora.util.Message;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.val;
import org.bukkit.command.CommandSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class DelayServiceImpl implements DelayService {

    MemoryService memoryService;

    @Override
    public boolean assertDelay(@NonNull CommandSender sender, @NonNull String key, long delay,
                            @NonNull TimeUnit delayUnit) {
        boolean b = false;
        val realKey = getDelayKey(sender, key);
        val now = System.currentTimeMillis();
        if (memoryService.containsKey(realKey)) {
            try {
                val lastDelay = memoryService.get(realKey);
                if (now - lastDelay <= delayUnit.toMillis(delay)) {
                    sender.spigot().sendMessage(new Message("&cAguarde para executar esta ação novamente.").formatted());
                    return !b;
                }
            } catch (ExecutionException ignored) { return true; }
        }
        memoryService.set(realKey, now, delay, delayUnit);
        return b;
    }

    @Override
    public void resetDelay(@NonNull CommandSender sender, @NonNull String key) {
        memoryService.remove(getDelayKey(sender, key));
    }

    @Override
    public boolean isInDelay(@NonNull CommandSender sender, @NonNull String key) {
        return memoryService.containsKey(getDelayKey(sender, key));
    }

    private String getDelayKey(CommandSender sender, String key) {
        return String.join(":", "delay", sender.getName().toLowerCase(), key);
    }
}

