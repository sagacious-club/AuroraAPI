package com.gitlab.pk7r.aurora.api.hook;

import com.gitlab.pk7r.aurora.hook.EconomyHook;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.OfflinePlayer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Lazy
@Service
@ConditionalOnClass(Economy.class)
public class EconomyHookImpl implements EconomyHook {

    Economy economy;

    @Override
    public void setEconomy(Economy economy) {
        this.economy = economy;
    }

    @Override
    public void deposit(OfflinePlayer player, BigDecimal amount) {
        economy.depositPlayer(player, amount.doubleValue());
    }

    @Override
    public void withdraw(OfflinePlayer player, BigDecimal amount) {
        economy.withdrawPlayer(player, amount.doubleValue());
    }

    @Override
    public void transfer(OfflinePlayer origin, OfflinePlayer destination, BigDecimal amount) {
        withdraw(origin, amount);
        deposit(destination, amount);
    }

    @Override
    public boolean has(OfflinePlayer player, BigDecimal amount) {
        return economy.has(player, amount.doubleValue());
    }

    @Override
    public void createAccount(OfflinePlayer player) {
        economy.createPlayerAccount(player);
    }

    @Override
    public String format(BigDecimal amount) {
        return economy.format(amount.doubleValue());
    }

    @Override
    public BigDecimal getBalance(OfflinePlayer player) {
        return BigDecimal.valueOf(economy.getBalance(player));
    }

    @Override
    public boolean hasAccount(OfflinePlayer player) {
        return economy.hasAccount(player);
    }

}