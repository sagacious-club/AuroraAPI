package com.gitlab.pk7r.aurora.api.service;

import com.gitlab.pk7r.aurora.AuroraPlugin;
import com.gitlab.pk7r.aurora.service.MemoryService;
import lombok.SneakyThrows;
import lombok.val;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.Closeable;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

@Service
public class MemoryServiceImpl implements Closeable, MemoryService {

    private Connection connection;

    private PreparedStatement readStatement, containsStatement, deleteStatement, insertStatement;

    @SneakyThrows
    @PostConstruct
    public void onEnable() {
        Class.forName("org.h2.Driver");
        val h2File = new File(AuroraPlugin.getAuroraPlugin().getDataFolder(), "memory").getAbsolutePath();
        val connection = DriverManager.getConnection("jdbc:h2:" + h2File);
        this.connection = connection;
        initTables(connection);
        purgeData(connection);
    }

    @SneakyThrows
    @Override
    public Long get(String key) {
        this.readStatement = connection.prepareStatement("select value from memory where key = ? and expires_at >= ?");
        readStatement.clearParameters();
        readStatement.setString(1, key);
        readStatement.setLong(2, System.currentTimeMillis());
        try (val resultSet = readStatement.executeQuery()) {
            if (resultSet.next()) {
                return resultSet.getLong("value");
            }
        }
        return null;
    }

    @Override
    @SneakyThrows
    public void remove(String key) {
        this.deleteStatement = connection.prepareStatement("delete from memory where key = ?");
        deleteStatement.clearParameters();
        deleteStatement.setString(1, key);
        deleteStatement.executeUpdate();
    }

    @SneakyThrows
    @Override
    public void set(String key, Long value, long expiration, TimeUnit expirationUnit) {
        this.insertStatement = connection.prepareStatement("insert into memory values (?, ?, ?)");
        val now = System.currentTimeMillis();
        remove(key);
        insertStatement.clearParameters();
        insertStatement.setString(1, key);
        insertStatement.setLong(2, value);
        insertStatement.setLong(3, now + expirationUnit.toMillis(expiration));
        insertStatement.executeUpdate();
    }

    @Override
    @SneakyThrows
    public boolean containsKey(String key) {
        this.containsStatement = connection.prepareStatement("select 1 from memory where key = ? and expires_at >= ?");
        containsStatement.clearParameters();
        containsStatement.setString(1, key);
        containsStatement.setLong(2, System.currentTimeMillis());
        try (val resultSet = containsStatement.executeQuery()) {
            return resultSet.next();
        }
    }

    @Override
    @SneakyThrows
    public void close() {
        readStatement.close();
        insertStatement.close();
        containsStatement.close();
        deleteStatement.close();
        connection.close();
    }

    private void initTables(Connection connection) throws SQLException {
        try (val statement = connection.createStatement()) {
            statement.executeUpdate("create table if not exists memory (key varchar(255) not null, value varchar(255) not null, " +
                    "expires_at bigint not null, primary key(key))");
        }
    }

    private void purgeData(Connection connection) throws SQLException {
        try (val statement = connection.prepareStatement("delete from memory where expires_at < ?")) {
            statement.setLong(1, System.currentTimeMillis());
            statement.executeUpdate();
        }
    }
}
