package com.gitlab.pk7r.aurora.api.hook;

import com.gitlab.pk7r.aurora.hook.ChatHook;
import net.milkbowl.vault.chat.Chat;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class ChatHookImpl implements ChatHook {

    Server server;

    Chat chat;

    @PostConstruct
    private void init() {
        chat = server.getServicesManager().getRegistration(Chat.class).getProvider();
    }

    @Autowired
    public ChatHookImpl(Server server) {
        this.server = server;
    }

    @Override
    public String getSuffix(Player player) {
        return chat.getPlayerSuffix(player);
    }

    @Override
    public String getPrefix(Player player) {
        return chat.getPlayerPrefix(player);
    }
}