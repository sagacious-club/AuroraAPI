package com.gitlab.pk7r.aurora.api.service;

import com.gitlab.pk7r.aurora.service.TemporaryCommandService;
import de.themoep.minedown.MineDown;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.GreedyStringArgument;
import org.bukkit.entity.Player;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.UUID;

@Component
public class TemporaryCommandServiceImpl extends TemporaryCommandService {

    @PostConstruct
    public void registerCommandOnInit() {
        new CommandAPICommand("chataction")
                .withArguments(new GreedyStringArgument("uuid"))
                .executes((sender, args) -> {
                    if (!(sender instanceof Player)) return;
                    UUID uuid = null;
                    try {
                        uuid = UUID.fromString((String) args[0]);
                    } catch (Throwable t) {
                        return;
                    }
                    if (!actions.containsKey(uuid)) {
                        sender.spigot().sendMessage(MineDown.parse("&cEssa ação já foi expirada ou usada."));
                        return;
                    }
                    Runnable r = actions.remove(uuid);
                    r.run();
                }).register();
    }
}