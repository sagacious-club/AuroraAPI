package com.gitlab.pk7r.aurora.spring;

import com.gitlab.pk7r.aurora.AuroraPlugin;
import com.gitlab.pk7r.aurora.execution.Execution;
import com.gitlab.pk7r.aurora.service.EventService;
import com.gitlab.pk7r.aurora.service.ModuleService;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ResourceLoader;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;
import java.util.concurrent.Executor;
import java.util.logging.Logger;

@Log4j2
@Configuration
@EnableScheduling
@EnableAsync
@EnableJpaRepositories(basePackages = { "com.gitlab.pk7r.aurora" })
@ComponentScan(basePackages = { "com.gitlab.pk7r.aurora" })
@EnableTransactionManagement
public class AuroraApplication {

    private boolean initialized = false;

    private final int databaseType = AuroraPlugin.getAuroraPlugin().getConfig().getConfigurationSection("database").getInt("type");
    @EventListener
    public void onEnable(ContextRefreshedEvent event) {
        if (initialized) return;
        initialized = true;
        val beans = event.getApplicationContext().getBeansOfType(Listener.class).values();
        val eventService = event.getApplicationContext().getBean(EventService.class);
        beans.forEach(eventService::registerEvents);

    }

    @Bean(name = "asyncExecutor")
    public Executor asyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(3);
        executor.setMaxPoolSize(3);
        executor.setQueueCapacity(100);
        executor.setThreadNamePrefix("AsynchThread-");
        executor.initialize();
        return executor;
    }

    @Bean
    public DataSource dataSource(@Value("${database.user}") String username,
                                 @Value("${database.pass}") String password,
                                 @Value("${database.url}") String url) {
        val dataSource = new DriverManagerDataSource();
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setUrl(url);
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
        val em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan("com.gitlab.pk7r.aurora");
        val vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        val properties = new Properties();
        if (databaseType == 0) {
            properties.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL92Dialect");
        } else {
            properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5InnoDBDialect");
        }
        em.setJpaProperties(properties);
        return em;
    }


    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
        val transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);
        return transactionManager;
    }

    @Bean
    @Primary
    public Flyway flyway(@Autowired ResourceLoader resourceLoader, @Autowired DataSource dataSource) {
        Flyway flyway = new Flyway(resourceLoader.getClassLoader());
        flyway.setDataSource(dataSource);
        flyway.setLocations(ModuleService.getModules().keySet().stream().map(module -> "/sql_" + module.toLowerCase()).toArray(String[]::new));
        flyway.setValidateOnMigrate(false);
        flyway.setBaselineOnMigrate(true);
        flyway.migrate();
        return flyway;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    @Bean(destroyMethod = "")
    public Plugin pluginBean() {
        return Bukkit.getPluginManager().getPlugin("Aurora");
    }

    @Bean(destroyMethod = "")
    public Server serverBean(Plugin plugin) {
        return plugin.getServer();
    }

    @Bean(destroyMethod = "")
    public BukkitScheduler schedulerBean(Server server) {
        return server.getScheduler();
    }

    @Bean(destroyMethod = "")
    public Logger loggerBean(Server server) {
        return server.getLogger();
    }

    @Bean(destroyMethod = "")
    public Execution executionBean() {
        return new Execution();
    }

}