package com.gitlab.pk7r.aurora.service;

import com.gitlab.pk7r.aurora.AuroraPlugin;
import com.gitlab.pk7r.aurora.Module;
import com.gitlab.pk7r.aurora.loader.ModuleLoader;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.bukkit.plugin.Plugin;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.File;
import java.util.*;

@Log4j2
@Component
public class ModuleService {

    @Getter
    private static final Map<String, String> modules = new HashMap<>();
    private DataSource dataSource;
    private boolean initialized = false;

    public static void registerModule(String name, String className) {
        modules.put(name, className);
    }

    public Set<String> getLoadedModules() {
        return Collections.unmodifiableSet(modules.keySet());
    }

    @EventListener
    @SneakyThrows
    public void init(ContextRefreshedEvent event) {
        if (initialized) return;
        initialized = true;
        initializeDependencies(event.getApplicationContext().getAutowireCapableBeanFactory());
        getLoadedModules().forEach(this::initializeModule);
    }

    private void initializeDependencies(BeanFactory beanFactory) {
        dataSource = beanFactory.getBean(DataSource.class);
    }

    private void initializeModule(String moduleName) {
        log.info("Inicializando módulo {}", moduleName);
    }

    private static Collection<Class<?>> loadModules(ClassLoader classLoader, File file) {
        val moduleLoader = new ModuleLoader(classLoader);
        val moduleFolder = new File(file, "modules");
        //noinspection ResultOfMethodCallIgnored
        moduleFolder.mkdirs();
        return moduleLoader.loadModules(moduleFolder);
    }

    public static void loadModules(Plugin plugin) {
        val modules = loadModules(plugin.getClass().getClassLoader(), plugin.getDataFolder());
        if (modules.isEmpty()) {
            plugin.getLogger().severe("Nenhum módulo foi encontrado, desabilitando plugin.");
            AuroraPlugin.selfDisable();
            return;
        }
        modules.forEach(module -> ModuleService.registerModule(module
                        .getAnnotation(Module.class).value()
                , module.getName()));
    }
}