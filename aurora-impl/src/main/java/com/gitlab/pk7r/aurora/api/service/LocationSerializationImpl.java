package com.gitlab.pk7r.aurora.api.service;

import com.gitlab.pk7r.aurora.service.LocationSerializationService;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.UUID;

@Service
class LocationSerializationImpl implements LocationSerializationService {

    @Override
    public String serializeLocation(Location location) {
        return String.format("%s;%s;%s;%s;%s;%s"
                , location.getX()
                , location.getY()
                , location.getZ()
                , location.getYaw()
                , location.getPitch()
                , Objects.requireNonNull(location.getWorld()).getName());
    }

    @Override
    public Location deserializeLocation(String location) {
        String [] parts = location.split(";");
        double x = Double.parseDouble(parts[0]);
        double y = Double.parseDouble(parts[1]);
        double z = Double.parseDouble(parts[2]);
        if (parts.length == 4) {
            String worldNameOrUUID = parts[3];
            World world;
            if (worldNameOrUUID.split("-").length == 5) {
                world = Bukkit.getWorld(UUID.fromString(worldNameOrUUID));
            } else {
                world = Bukkit.getWorld(worldNameOrUUID);
            }
            return new Location(world, x, y, z);
        } else {
            float yaw = Float.parseFloat(parts[3]);
            float pitch = Float.parseFloat(parts[4]);
            String worldNameOrUUID = parts[5];
            World world;
            if (worldNameOrUUID.split("-").length == 5) {
                world = Bukkit.getWorld(UUID.fromString(worldNameOrUUID));
            } else {
                world = Bukkit.getWorld(worldNameOrUUID);
            }
            return new Location(world, x, y, z, yaw, pitch);
        }
    }
}