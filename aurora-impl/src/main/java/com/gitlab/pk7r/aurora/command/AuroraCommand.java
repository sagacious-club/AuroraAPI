package com.gitlab.pk7r.aurora.command;

import com.gitlab.pk7r.aurora.service.ModuleService;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPICommand;
import lombok.val;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.stream.Collectors;

@Component
public class AuroraCommand {

    @PostConstruct
    public void auroraCommand() {
        new CommandAPICommand("aurora")
                .executesPlayer((player, objects) -> {
                    player.spigot().sendMessage(new Message("&a&lAurora\n").formatted());
                    player.sendMessage(" ");
                    val message = ModuleService.getModules()
                            .keySet()
                            .stream()
                            .map(module -> "&6" + module.substring(0, 1).toUpperCase() + module.substring(1))
                            .collect(Collectors.collectingAndThen(Collectors.joining("&e, &6"),
                                    result -> result.isEmpty() ?
                                            "&cNenhum módulo encontrado."
                                            :
                                            "&aModulos ativos:\n" + result));
                    player.spigot().sendMessage(new Message(message).formatted());
                })
                .register();
    }
}
