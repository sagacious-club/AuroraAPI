package com.gitlab.pk7r.aurora.spring.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class StubEntity {

    @Id
    private Integer id;

}