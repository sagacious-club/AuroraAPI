package com.gitlab.pk7r.aurora;

import com.gitlab.pk7r.aurora.loader.CompoundClassLoader;
import com.gitlab.pk7r.aurora.service.ModuleService;
import com.gitlab.pk7r.aurora.spring.AuroraApplication;
import com.gitlab.pk7r.aurora.spring.ConfigurationSource;
import com.gitlab.pk7r.aurora.util.CommandUtil;
import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.CommandAPIConfig;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;

import java.util.ArrayList;
import java.util.List;

public class AuroraPlugin extends JavaPlugin {

    @Getter @Setter
    private static AuroraPlugin auroraPlugin;

    @Getter @Setter
    private static AnnotationConfigApplicationContext applicationContext;

    public void onLoad() {
        saveDefaultConfig();
        setAuroraPlugin(this);
        CommandAPI.onLoad(new CommandAPIConfig().verboseOutput(true).silentLogs(true));
    }

    public void onEnable() {
        CommandAPI.onEnable(getAuroraPlugin());
        try {
            loadDataSourceDriver();
            getVault();
            startPlugin();
        } catch (Throwable throwable) {
            getLogger().severe("Falha ao inicializar a Aurora.");
            throwable.printStackTrace();
            selfDisable();
        }
    }

    public void onDisable() {
        if (getApplicationContext() != null) {
            getApplicationContext().close();
        }
    }

    private void startPlugin() {
        CommandUtil.unregisterCommands(getConfig().getStringList("disabled_cmds"));
        List<ClassLoader> springClassLoaders = new ArrayList<>();
        springClassLoaders.add(getClassLoader());
        springClassLoaders.add(Thread.currentThread().getContextClassLoader());
        ClassLoader classLoader = new CompoundClassLoader(springClassLoaders);
        ResourceLoader loader = new DefaultResourceLoader(classLoader);
        ModuleService.loadModules(getAuroraPlugin());
        val annotationConfigApplicationContext = new AnnotationConfigApplicationContext();
        setApplicationContext(annotationConfigApplicationContext);
        applicationContext.setResourceLoader(loader);
        applicationContext.addBeanFactoryPostProcessor(new ConfigurationSource(this.getConfig()));
        applicationContext.register(AuroraApplication.class);
        applicationContext.refresh();
    }

    @SneakyThrows
    private void loadDataSourceDriver() {
        val type = this.getConfig().getConfigurationSection("database").getInt("type");
        if (type == 0 ) {
            Class.forName("org.postgresql.Driver");
        } else {
            Class.forName("com.mysql.cj.jdbc.Driver");
        }
    }

    public static void selfDisable() {
        try {
            Bukkit.getServer().getPluginManager().disablePlugin(getAuroraPlugin());
        } catch (Exception ignored) {
        }
    }

    @SneakyThrows
    private void getVault() {
        if (Bukkit.getServer().getPluginManager().getPlugin("Vault") == null) {
            throw new NullPointerException("Vault não foi encontrado, desligando plugin.");
        }
    }
}