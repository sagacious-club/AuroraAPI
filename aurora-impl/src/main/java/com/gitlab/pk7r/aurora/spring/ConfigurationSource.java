package com.gitlab.pk7r.aurora.spring;

import lombok.NonNull;
import org.bukkit.configuration.file.FileConfiguration;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import java.util.Optional;
import java.util.Properties;

@SuppressWarnings("deprecation")
public class ConfigurationSource extends PropertyPlaceholderConfigurer {

    private final FileConfiguration fileConfiguration;

    public ConfigurationSource(FileConfiguration fileConfiguration) {
        this.fileConfiguration = fileConfiguration;
    }

    @Override
    protected String resolvePlaceholder(@NonNull String placeholder, @NonNull Properties props) {
        return Optional.ofNullable(fileConfiguration.getString(placeholder))
                .orElseGet(() -> super.resolvePlaceholder(placeholder, props));
    }
}